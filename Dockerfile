FROM openjdk:8-alpine
RUN apk --no-cache add curl
ADD target/crud-jpa-vuejs-1.2-SNAPSHOT.jar crud-jpa-vuejs-1.2-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "crud-jpa-vuejs-1.2-SNAPSHOT.jar"]
