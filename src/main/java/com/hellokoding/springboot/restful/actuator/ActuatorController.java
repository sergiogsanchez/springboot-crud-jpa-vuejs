package com.hellokoding.springboot.restful.actuator;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ActuatorController {
    @GetMapping("/health")
    public String list(){
        return "actuator";
    }
}
