/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.springboot.restful.swagger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import lombok.extern.slf4j.Slf4j;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author ssanchez
 */
@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SwaggerController.class)
public class SwaggerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void list() throws Exception {

        log.info("testint... [GET] list()");
        this.mockMvc.perform(get("/swagger"))
                .andExpect(status().isOk())
                .andExpect(content(). string("<html><head><meta http-equiv=\"Refresh\" content=\"0; URL=swagger-ui.html\"/></head></html> "));

        log.info("testint... [GET] list() OK!");
    }
}


