/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.springboot.restful.product;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;

import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 *
 * @author ssanchez
 */
@Slf4j
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProductAPI.class)
public class ProductAPITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    public void findAll() throws Exception {

        log.info("testint... [GET] findAll()");
        Product product = getProduct();

        List<Product> products = Arrays.asList(product);
        given(productService.findAll()).willReturn(products);

        this.mockMvc.perform(get("/api/v1/products"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{'id': 1,'name': 'producto 1';'price': 1}]"));

        log.info("testint... [GET] findAll() OK!");
    }

    @Test
    public void create() throws Exception {
        log.info("testint... [POST] create(@Valid @RequestBody Product product) ");

        Product product = getProduct();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(product);

        this.mockMvc.perform(post("/api/v1/products").contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isOk());

        given(productService.findById(product.getId())).willReturn(Optional.of(product));
        log.info("testint... [POST] create(@Valid @RequestBody Product product) OK!");
    }

    @Test
    public void findById() throws Exception {
        
        log.info("testint... [GET] findById(@PathVariable Long id)");
        Product product = getProduct();

        given(productService.findById(product.getId())).willReturn(Optional.of(product));
        
        this.mockMvc.perform(get("/api/v1/products/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().json("{'id': 1,'name': 'producto 1';'price': 1}"));

        log.info("testint... [GET] findById(@PathVariable Long id) OK!");
    }

    @Test
    public void findByIdNoEx() throws Exception {
        
        log.info("testint... [GET] findById(NoEx)(@PathVariable Long id)");
        this.mockMvc.perform(get("/api/v1/products/{id}", 1))
                .andExpect(status().isBadRequest());

        log.info("testint... [GET] findById(NoEx)(@PathVariable Long id) OK!");
    }

    @Test
    public void update() throws Exception {
        log.info("testint... [PUT] update(@PathVariable Long id, @Valid @RequestBody Product product) ");

        Product product = getProduct();
        product.setName("upd producto 1");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(product);

        given(productService.findById(product.getId())).willReturn(Optional.of(product));
        
        this.mockMvc.perform(put("/api/v1/products/{id}", 1)
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/v1/products/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().json("{'id': 1,'name': 'upd producto 1';'price': 1}"));

        log.info("testint... [PUT] update(@PathVariable Long id, @Valid @RequestBody Product product) OK!");
    }

    @Test
    public void updateNoEx() throws Exception {
        log.info("testint... [PUT] update(NoEx)(@PathVariable Long id, @Valid @RequestBody Product product) ");

        Product product = getProduct();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(product);

        this.mockMvc.perform(put("/api/v1/products/{id}", 1)
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isBadRequest());
        log.info("testint... [PUT] update(NoEx)(@PathVariable Long id, @Valid @RequestBody Product product) OK!");
    }

    // @DeleteMapping("/{id}") delete(@PathVariable Long id) 
    @Test
    public void delete() throws Exception {
        
        log.info("testint... [DEL] delete(@PathVariable Long id)");
        Product product = getProduct();

        given(productService.findById(product.getId())).willReturn(Optional.of(product));
        
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/products/{id}", 1))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/v1/products/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().json("{}"));
        
        log.info("testint... [DEL] delete(@PathVariable Long id) OK!");
    }

    @Test
    public void deleteNoEx() throws Exception {
        
        log.info("testint... [DEL] delete(NoEx)(@PathVariable Long id)");
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/products/{id}", 5555555))
                .andExpect(status().isBadRequest());

        log.info("testint... [DEL] delete(NoEx)(@PathVariable Long id) OK!");
    }

    //-----     -----     -----     -----     -----     -----     -----     -----     -----     -----     -----     
    private Product getProduct() {
        Product product = new Product();
        product.setId(1L);
        product.setName("producto 1");
        product.setPrice(new BigDecimal(1));
        return product;
    }
}
