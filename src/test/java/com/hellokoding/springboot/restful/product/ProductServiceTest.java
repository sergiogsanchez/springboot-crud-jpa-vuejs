/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.springboot.restful.product;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;

import static org.assertj.core.api.Assertions.assertThat;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.doReturn;

/**
 *
 * @author ssanchez
 */
@Slf4j
@RunWith(SpringRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService productService;
    @Mock
    private ProductRespository productRespository;

    @Test
    public void findAll() throws Exception {

        log.info("testint... findAll()");
        Product product = getProduct();
        List<Product> expectedProducts = Arrays.asList(product);

        doReturn(expectedProducts).when(productRespository).findAll();

        List<Product> findAll = productService.findAll();

        assertThat(findAll).isEqualTo(expectedProducts);
        assertThat(findAll).hasSize(1);
        assertThat(findAll).element(0).hasFieldOrPropertyWithValue("id", 1L);

        log.info("testint... findAll() OK!");
    }

    @Test
    public void findById() throws Exception {

        log.info("testint... findById()");
        Product product = getProduct();

        doReturn(Optional.of(product) ).when(productRespository).findById(1L);

        Optional<Product> findById = productService.findById(1L);

        assertThat(findById.get()).isEqualTo(product);
        assertThat(findById.get().getId()).isEqualTo(1L);

        log.info("testint... findById() OK!");
    }

    @Test
    public void save() throws Exception {

        log.info("testint... save(Product product)");
        Product product = getProduct();

        doReturn(product).when(productRespository).save(product);

        Product save = productService.save(product);

        assertThat(save).isEqualTo(product);
        assertThat(save.getId()).isEqualTo(1L);

        log.info("testint... save(Product product) OK!");
    }

    @Test
    public void deleteById() throws Exception {

        log.info("testint... deleteById(Long id)");
        productService.deleteById(1L);

        log.info("testint... deleteById(Long id) OK!");
    }

    
    //-----     -----     -----     -----     -----     -----     -----     -----     -----     -----     -----     
    private Product getProduct() {
        Product product = new Product();
        product.setId(1L);
        product.setName("producto 1");
        product.setPrice(new BigDecimal(1));
        return product;
    }
}
